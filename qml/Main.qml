/*
 * Copyright (C) 2020  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * urldispatchertest is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'urldispatchertest.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('UrlDispatcher Test')
        }

        Label {
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            text: i18n.tr('Version ') + Qt.application.version + i18n.tr('\n Name ') + Qt.application.name  + i18n.tr('\n  platform ') + Qt.platform.os

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter
        }
    }

    //Handle incoming otpauth:// url
    Connections {
        target: UriHandler

        onOpened: {
            console.log("UriHandler signal triggered-------------------")

            if (uris.length > 0) {
                console.log("uri received:", uris[0])
            }
        }
    }

    Component.onCompleted: {
        console.log("onCompleted----------------")
        if (Qt.application.arguments) {
            console.log("Qt.application.arguments exists")
            if (Qt.application.arguments.length > 0) {
                console.log("Qt.application.arguments is > 0")
                for (var i = 0; i < Qt.application.arguments.length; i++) {
                    console.log("Qt.application.arguments",i,"is",Qt.application.arguments[i])
                }
            }
        }
    }
}
